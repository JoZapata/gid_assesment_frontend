var keyMirror = require('keymirror');

var APIRoot;

/*
if (process.env.NODE_ENV === 'development') {
  APIRoot = "http://localhost:8080";
  //APIRoot = 'http://localhost:8080/';
} else if (process.env.NODE_ENV === 'staging') {
  APIRoot = "https://api-staging.obsidianwallet.com";
} else {
  APIRoot = "https://api-staging.obsidianwallet.com";
}
*/

// APIRoot = "http://localhost:8080"; //deploy
APIRoot = "https://morning-shelf-52517.herokuapp.com"; //release


module.exports = {
  
  APIRoot: APIRoot,
  
  APIEndpoints: {
    OPPORTUNITIES: APIRoot + "/opportunities"
  },
  
  
  PayloadSources: keyMirror({
    SERVER_ACTION: null,
    VIEW_ACTION: null
  }),
  
  api: keyMirror({
    REQUEST_OPPORTUNITY: null,
    REQUEST_OPPORTUNITIES: null,
    SEARCH_OPPORTUNITIES: null,
    UPDATE_OPPORTUNITY: null,
    OPPORTUNITIES_SUCCESS: null,
    OPPORTUNITIES_FAILURE: null,
  }),
  
  request: keyMirror({
    PENDING: null
  }),
  
  ActionTypes: keyMirror({
    // Session
  }),
  
};
