var Immutable = require('immutable');

var AppDispatcher = require('../dispatcher/AppDispatcher.js');
var AppConstants = require('../constants/AppConstants.js');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');

var OpportunitiesActionCreators = require('../actions/OpportunitiesActionCreators');


var CHANGE_EVENT = 'change';

var Opportunity = Immutable.Record({
  id: null,
  title: null,
  description: null
});

var _opportunities = [];
var _pendingRequest = false;
var _editingOpp = false;
var _editOppId = null;

var OpportunitiesStore = assign({}, EventEmitter.prototype, {
  
  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },
  
  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback);
  },
  
  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  },
  
  clearOpportunities: function() {
    _opportunities = {};
  },
  
  persistOpportunitiesRequest: function(response) {
    if (response === AppConstants.request.PENDING) {
      _pendingRequest = true;
    } else {
      _pendingRequest = false;
      _opportunities = response.data;
    }
  },
  
  persistOpportunitiesSearch: function(response) {
    if (response === AppConstants.request.PENDING) {
      _pendingRequest = true;
    } else {
      _pendingRequest = false;
      OpportunitiesStore.clearOpportunities();
      
      if (response.data.length <= 10) {
        response.data.map(function(opp) {
          _opportunities[opp.id] = assign(_opportunities[opp.id] || {}, opp);
          OpportunitiesActionCreators.requestOpportunity(opp.id);
        });
      } else {
        response.data.map(function(opp) {
          _opportunities[opp.id] = assign(_opportunities[opp.id] || {}, opp);
        });
      }
    }
  },
  
  persistOpportunity: function(response) {
    if (response === AppConstants.request.PENDING) {
      _pendingRequest = true;
    } else {
      _pendingRequest = false;
      _opportunities[response.data.id] = assign(_opportunities[response.data.id] || {}, response.data);
      _opportunities[response.data.id].fetched = true;
    }
  },
  
  persistOpportunityUpdate: function(id, diff) {
    if (diff === AppConstants.request.PENDING) {
      _pendingRequest = true;
    } else {
      _pendingRequest = false;
      var _opportunity = _opportunities[id];
      if (!_opportunity) {
        _opportunity = {};
      }
      
      for (var key in diff) {
        if (diff.hasOwnProperty(key)) {
          _opportunity[key] = diff[key];
        }
      }
      _opportunities[id] = _opportunity;
    }
  },
  
  setEdit: function(id, editingOpp){
    _editingOpp = editingOpp;
    _editOppId = id;
  },
  
  getEditingOpp: function(){
    return _editOppId;
  },
  
  isEditing: function(){
    return _editingOpp
  },
  
  /*
  
  opportunitiesSucceeded: function(id) {
    for (var i in _servicePayments) {
      if (_servicePayments[i].id === id) {
        _servicePayments[i].state = "Succeeded";
        break;
      }
    }
  },
  
  opportunitiesFailed: function(id) {
    for (var i in _servicePayments) {
      if (_servicePayments[i].id === id) {
        _servicePayments[i].state = "Failed";
        break;
      }
    }
  },
  */
  
  getOpportunities: function() {
    return _opportunities.map(function(opp) {
      return new Opportunity(opp);
    });
  },
  
  getOpportunity: function(id) {
    if (_opportunities[id]) {
      return new Opportunity(_opportunities[id]);
    } else {
      return new Opportunity({});
    }
  },
  
  getRequestPending: function() {
    return _pendingRequest;
  }
});

OpportunitiesStore.dispatchToken = AppDispatcher.register(function(payload) {
  var action = payload.action;
  
  switch(action.type) {
    
    case AppConstants.api.REQUEST_OPPORTUNITIES:
      OpportunitiesStore.persistOpportunitiesRequest(action.response);
      OpportunitiesStore.emitChange();
      break;
    
    case AppConstants.api.OPPORTUNITIES_SUCCESS:
      OpportunitiesStore.opportunitiesSucceeded(action.id);
      OpportunitiesStore.emitChange();
      break;
    
    case AppConstants.api.OPPORTUNITIES_FAILURE:
      OpportunitiesStore.opportunitiesFailed(action.id);
      OpportunitiesStore.emitChange();
      break;
  
    case AppConstants.api.SEARCH_OPPORTUNITIES:
      OpportunitiesStore.persistOpportunitiesSearch(action.response);
      OpportunitiesStore.emitChange();
      break;
  
    case AppConstants.api.REQUEST_OPPORTUNITY:
      OpportunitiesStore.persistOpportunity(action.response);
      OpportunitiesStore.emitChange();
      break;
  
    case AppConstants.api.UPDATE_OPPORTUNITY:
      OpportunitiesStore.persistOpportunityUpdate(action.id, action.response);
      OpportunitiesStore.emitChange();
      break;
    
    default:
  }
  
  return true;
});

module.exports = OpportunitiesStore;


// SEARCH_OPPORTUNITIES
