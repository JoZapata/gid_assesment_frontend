var ServerActionCreators = require('../actions/ServerActionCreators.js');
var request = require('superagent');
var AppConstants = require('../constants/AppConstants.js');
var AppDispatcher = require('../dispatcher/AppDispatcher');

var _pendingRequests = {};

function abortPendingRequests(key) {
  if (_pendingRequests[key]) {
    _pendingRequests[key]._callback = function(){};
    _pendingRequests[key].abort();
    _pendingRequests[key] = null;
  }
}

module.exports = {
  
  
  requestOpportunities: function() {
    
    var key = AppConstants.api.REQUEST_OPPORTUNITIES;
    
    abortPendingRequests(key);
    
    _pendingRequests[key] = request.get(AppConstants.APIEndpoints.OPPORTUNITIES)
    .set('Accept', 'application/json')
    .end(function(error, res){
      if (res) {
        var json = JSON.parse(res.text);
        ServerActionCreators.recieveOpportunities(json.data);
      }
    });
  },
  
  searchOpportunities: function(data) {
    
    var key = AppConstants.api.REQUEST_OPPORTUNITIES;
    abortPendingRequests(key);
    
    key = AppConstants.api.SEARCH_OPPORTUNITIES;
    abortPendingRequests(key);
    
    ServerActionCreators.opportunitiesSearchRequesting();
    
    _pendingRequests[key] = request.get(AppConstants.APIEndpoints.OPPORTUNITIES)
    .set('Accept', 'application/json')
    .query({
      queryText: data.query,
      foodCovered: data.foodCovered,
      transportationCovered: data.transportationCovered,
      accomodationCovered: data.accomodationCovered
    })
    .end(function(error, res){
      if (res) {
        var json = JSON.parse(res.text);
        ServerActionCreators.opportunitiesSearchSuccess(json);
      }
    });
  },
  
  requestOpportunity: function(id) {
    
    var key = AppConstants.api.REQUEST_OPPORTUNITY + id;
    abortPendingRequests(key);
    
    _pendingRequests[key] = request.get(AppConstants.APIEndpoints.OPPORTUNITIES + "/" + id)
    .set('Accept', 'application/json')
    .end(function(error, res){
      if (res) {
        var json = JSON.parse(res.text);
        ServerActionCreators.opportunityRequestSuccess(id, json);
      }
    });
  },
  
  updateOpportunity: function(id,diff) {
    ServerActionCreators.opportunityUpdating(id);
  
    request.patch(AppConstants.APIEndpoints.OPPORTUNITIES + "/" + id)
    .set('Accept', 'application/json')
    .send({diff})
    .end(function(error, res){
      if (res) {
          var json = JSON.parse(res.text);
          ServerActionCreators.opportunityUpdated(id, diff);
      }
    });
  }
  
};
