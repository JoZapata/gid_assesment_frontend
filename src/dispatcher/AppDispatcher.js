

var AppConstants = require('../constants/AppConstants.js');
var Dispatcher = require('flux').Dispatcher;
var assign = require('object-assign');

var PayloadSources = AppConstants.PayloadSources;

var SupportAppDispatcher = assign(new Dispatcher(), {
  
  handleServerAction: function(action) {
    console.log(action);
    var payload = {
      source: PayloadSources.SERVER_ACTION,
      action: action
    };
    this.dispatch(payload);
  },
  
  handleViewAction: function(action) {
    console.log(action);
    var payload = {
      source: PayloadSources.VIEW_ACTION,
      action: action
    };
    this.dispatch(payload);
  }
});

module.exports = SupportAppDispatcher;
