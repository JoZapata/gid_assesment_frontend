import React, { Component } from 'react';
import '../styles/OpportunitiesSearchPage.css';
import OpportunitiesSearch from './OpportunitiesSearch';

var OpportunitiesActionCreators = require('../actions/OpportunitiesActionCreators');
var OpportunitiesStore = require('../stores/OpportunitiesStore');

class OpportunitiesSearchPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      requestPending: null,
      data: []
    };
  }
  
  componentWillMount = () => {
    OpportunitiesActionCreators.requestOpportunities();
  };
  
  componentDidMount = () => {
    OpportunitiesStore.addChangeListener(this._onChange);
  };
  
  componentWillUnmount = () => {
    OpportunitiesStore.removeChangeListener(this._onChange);
  };
  
  _onChange = () => {
    this.setState({
      data: OpportunitiesStore.getOpportunities(),
      requestPending: OpportunitiesStore.getRequestPending()
    });
  };
  
  render = () => {
  
    const { requestPending, data} = this.state;
    
    
    var content = '';
    if(requestPending !== null){
      content = (
        <OpportunitiesSearch
          data={data}
        />
      );
    }else{
      content = (
        <h2>LOADING...</h2>
      );
    }
    
    return (
      <div className="OpportunitiesSearchPage">
        {content}
      </div>
    );
  };
}

export default OpportunitiesSearchPage;
