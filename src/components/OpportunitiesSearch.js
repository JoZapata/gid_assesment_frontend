import React, { Component } from 'react';
import '../styles/OpportunitiesSearchPage.css';

var OpportunitiesActionCreators = require('../actions/OpportunitiesActionCreators');
var OpportunitiesStore = require('../stores/OpportunitiesStore');

function debounce(func, wait, immediate) {
  var timeout;
  return function() {
    var context = this, args = arguments;
    var later = function() {
      timeout = null;
      if (!immediate) { func.apply(context, args); }
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) { func.apply(context, args); }
  };
}

class OpportunitiesSearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      query: '',
      foodCovered: null,
      transportationCovered: null,
      accomodationCovered: null
    };
  }

  search = (data) =>{
    this.setState({
      query: data.query,
      foodCovered: data.enrollment,
      transportationCovered: data.transportationCovered,
      accomodationCovered: data.accomodationCovered
    });
    
    function update(data) {
      OpportunitiesActionCreators.searchOpportunities(data);
    }
    debounce(update(data), 250);
  };
  
  render = () =>{
    return (
      <div>
        <h2>Opportunities Lookup</h2>
        <fieldset>
          <SearchBox
            query={this.state.query}
            foodCovered={this.state.foodCovered}
            transportationCovered={this.state.transportationCovered}
            accomodationCovered={this.state.accomodationCovered}
            search={this.search}
          />
        </fieldset>
        <DisplayTable data={this.props.data}/>
      </div>
    );
  }
}

export default OpportunitiesSearch;

class SearchBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      foodCovered: 'disabled',
      transportationCovered: 'disabled',
      accomodationCovered: 'disabled'
    };
  }
  
  search = (e) =>  {
    e.preventDefault();
    var query = this.refs.searchInput.value;
    this.props.search({
      query: query,
      foodCovered: this.state.foodCovered,
      transportationCovered: this.state.transportationCovered,
      accomodationCovered: this.state.accomodationCovered
    });
  };
  
  handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    
    console.log(value);
    console.log(name);
    
    this.setState({
      [name]: value
    });
  };
  
  render = () => {
    
    return <form className="search-form" onSubmit={this.search}>
      <div className='small-12 columns'>
        <input type="search" ref="searchInput" placeholder="Search Title"/>
      </div>
  
  
      <div className='small-12 columns'>
        <label className='small-4 columns'>
          <h5>Food Covered</h5>
    
          <select name="foodCovered" value={this.state.foodCovered} onChange={this.handleInputChange}>
            <option value="active">Yes</option>
            <option value="inactive">No</option>
            <option value="disabled">Disabled</option>
          </select>
        </label>
    
        <label className='small-4 columns'>
          <h5>Transportation Covered</h5>
    
          <select name="transportationCovered" value={this.state.transportationCovered} onChange={this.handleInputChange}>
            <option value="active">Yes</option>
            <option value="inactive">No</option>
            <option value="disabled">Disabled</option>
          </select>
        </label>
    
        <label className='small-4 columns'>
          <h5>Accomodation Covered</h5>
    
          <select name="accomodationCovered" value={this.state.accomodationCovered} onChange={this.handleInputChange}>
            <option value="active">Yes</option>
            <option value="inactive">No</option>
            <option value="disabled">Disabled</option>
          </select>
        </label>
      </div>
  
      <br/>
  
      <div className='small-12 columns'>
        <button onClick={this.search} className="button">Search</button>
      </div>
    </form>;
  };
}

class OpportunityRow extends Component {
  constructor(props) {
    super(props);
    
    var opp = this.props.opp;
    this.state = {
      requestPending: null,
      isEditing: false,
      opp: opp,
      diff: {
        title: opp.title,
        description: opp.description
      }
    }
  }
  
  componentWillMount = () => {
    OpportunitiesActionCreators.requestOpportunity(this.props.opp.id);
  };
  
  componentDidMount = () => {
    OpportunitiesStore.addChangeListener(this._onChange);
  };
  
  componentWillUnmount = () => {
    OpportunitiesStore.removeChangeListener(this._onChange);
  };
  
  _onChange = () => {
    this.setState({
      opp: OpportunitiesStore.getOpportunity(this.props.opp.id),
      requestPending: OpportunitiesStore.getRequestPending()
    });
  };
  
  handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    
    var diff = this.state.diff;
    
    diff[name] = value;
    
    this.setState({
      diff: diff
    });
  };
  
  setEdit = (e) => {
    var updatedOpp = OpportunitiesStore.getOpportunity(this.props.opp.id);
    
    let target = e.target.id;
    OpportunitiesStore.setEdit(target, true);
    this.setState({
      isEditing: true,
      opp: updatedOpp,
      diff:{
        title: updatedOpp.title,
        description: updatedOpp.description
      }
    })
  };
  
  saveChanges = () => {
    var diff = this.state.diff;
    if(
      (diff.title !== '')
    ){
      OpportunitiesActionCreators.updateOpportunity(this.props.opp.id, diff);
      this.setState({
        isEditing: false
      })
    }else{
      alert('Title must not be empty')
    }
    
  };
  
  cancelEdit = () => {
    var opp = this.state.opp;
    this.setState({
      isEditing: false,
      diff: {
        title: opp.title,
        description: opp.description
      }
    })
  };
  
  render() {
    var opp = this.props.opp;
    var diff = this.state.diff;
    
    var content = '';
    
    if(this.state.requestPending === null){
      content = (
        <tr key={opp.id}>
          <td>
            LOADING...
          </td>
          <td>
            ...
          </td>
        </tr>
      );
      
    }else if(this.state.isEditing){
      
      content = (
        <tr key={opp.id}>
          <td>
            <div>
              <div className='small-12 columns'>
                <label className='small-4 columns'>
                  Title:
                  &nbsp;
                  <input type="text" name="title" value={diff.title} onChange={this.handleInputChange} />
                </label>
    
                <br/><br/>
                
                <label className='small-8 columns'>
                  Description:
                  &nbsp;
                  <textarea name="description" value={diff.description} onChange={this.handleInputChange} />
                </label>
              </div>
  
              <br/>
  
              <div className='small-12 columns'>
                <button onClick={this.saveChanges} className="button">Save</button>
              </div>
              
            </div>
          </td>
          <td>
            <div className='small-12 columns'>
              <button onClick={this.cancelEdit} className="button">Cancel</button>
            </div>
          </td>
        </tr>
      );
    }else{
      content = (
        <tr key={opp.id}>
          <td>
            {opp.title}
          </td>
          <td>
            <a href="#" ><i id={opp.id} onClick={this.setEdit} className="fa fa-times">Edit</i></a>
          </td>
        </tr>
      );
    }
    
    return (content);
  }
}

class DisplayTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      viewport: {
        top: 0,
        height: 0
      }
    }
  }
  
  componentDidMount = () => {
    window.addEventListener('scroll', this.updateViewport, false);
    window.addEventListener('resize', this.updateViewport, false);
    this.updateViewport();
  };
  
  componentWillUnmount = () => {
    window.removeEventListener('scroll', this.updateViewport);
    window.removeEventListener('resize', this.updateViewport);
  };
  
  updateViewport = () => {
    function update(component) {
      component.setState({
        viewport: {
          top: window.pageYOffset,
          height: window.innerHeight
        }
      });
    }
    debounce(update(this));
  };
  
  render = () => {
    var rows=[];
    var viewport = this.state.viewport;
    
    this.props.data.forEach(function(opp) {
      var row = (
        <OpportunityRow
          key={opp.id}
          opp={opp}
          viewport={viewport}
        />
      );
      
      var filter = rows.filter(rw => parseInt(rw.key) === parseInt(opp.id));
      if(filter.length === 0){
        rows.push(row);
      }
    });
    
    //returning the table
    return(
      <table>
        <thead>
        <tr>
          <th>Title</th>
          <th>Info</th>
        </tr>
        </thead>
        <tbody>
        {rows}
        </tbody>
      </table>
    );
  }
}
