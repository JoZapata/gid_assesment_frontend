var AppDispatcher = require('../dispatcher/AppDispatcher');
var AppConstants = require('../constants/AppConstants');

module.exports = {
  recieveOpportunities: function(json) {
    AppDispatcher.handleServerAction({
      type: AppConstants.api.REQUEST_OPPORTUNITIES,
      response: json
    });
  },
  
  opportunitiesFailure: function(json) {
    AppDispatcher.handleServerAction({
      type: AppConstants.api.OPPORTUNITIES_FAILURE,
      id: json.id
    });
  },
  
  opportunitiesSuccess: function(json) {
    AppDispatcher.handleServerAction({
      type: AppConstants.api.OPPORTUNITIES_SUCCESS,
      id: json.id
    });
  },
  
  opportunitiesSearchRequesting: function() {
    AppDispatcher.handleServerAction({
      type: AppConstants.api.SEARCH_OPPORTUNITIES,
      response: AppConstants.request.PENDING
    });
  },
  
  opportunitiesSearchSuccess: function(response) {
    AppDispatcher.handleServerAction({
      type: AppConstants.api.SEARCH_OPPORTUNITIES,
      response: response
    });
  },
  
  opportunityRequestSuccess: function(id, response) {
    AppDispatcher.handleServerAction({
      type: AppConstants.api.REQUEST_OPPORTUNITY,
      id: id,
      response: response
    });
  },
  
  opportunityUpdating: function(id) {
    AppDispatcher.handleServerAction({
      type: AppConstants.api.UPDATE_OPPORTUNITY,
      id: id,
      response: AppConstants.request.PENDING
    });
  },
  
  opportunityUpdated: function(id, diff) {
    AppDispatcher.handleServerAction({
      type: AppConstants.api.UPDATE_OPPORTUNITY,
      id: id,
      response: diff
    });
  },
};
