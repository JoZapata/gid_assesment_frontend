import React, { Component } from 'react';
import '../styles/App.css';
import OpportunitiesList from './OpportunitiesSearchPage'

class App extends Component {
  render = () => {
  
    return (
      <div className="App">
        <header className="App-header">
          TITLE
        </header>
        <div className="App-content">
          <OpportunitiesList/>
        </div>
      </div>
    );
  };
}

export default App;
