var AppDispatcher = require('../dispatcher/AppDispatcher.js');
var AppConstants = require('../constants/AppConstants.js');
var WebAPIUtils = require('../utils/WebAPIUtils.js');

module.exports = {
  
  requestOpportunities: function() {
    WebAPIUtils.requestOpportunities();
  },
  
  searchOpportunities: function(data) {
    WebAPIUtils.searchOpportunities(data);
  },
  
  requestOpportunity: function(id) {
    WebAPIUtils.requestOpportunity(id);
  },
  
  updateOpportunity: function(id,diff){
    WebAPIUtils.updateOpportunity(id,diff);
  }
  
  /*
  success: function(id, confirmationCode) {
    WebAPIUtils.servicePaymentSuccess(id, confirmationCode);
  },
  
  failure: function(id, error) {
    WebAPIUtils.servicePaymentFailure(id, error);
  }
  */
  
};
